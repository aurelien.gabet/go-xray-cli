package types

import (
	"net/http"
	"time"
)

type GetToken struct {
	Name     string `json:"name,omitempty"`
	Password string `json:"password,omitempty"`
}

type GetTokenBody struct {
	Token    string `json:"token"`
	Admin    bool   `json:"admin"`
	UserName string `json:"userName"`
	Email    string `json:"email"`
}

type CheckForRetry func(resp *http.Response, err error) (bool, error)

type Backoff func(min, max time.Duration, attemptNum int, resp *http.Response) time.Duration
