package xray

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/aurelien.gabet/go-xray-cli/internal/utils"
	"gitlab.com/aurelien.gabet/go-xray-cli/pkg/types"

	"path"

	"github.com/google/go-querystring/query"
)

type Service struct {
	Client *Client
}

type Client struct {
	Client        *http.Client
	Credentials   *types.GetToken
	RetryWaitMin  time.Duration
	RetryWaitMax  time.Duration
	RetryMax      int
	CheckForRetry types.CheckForRetry
	Backoff       types.Backoff
	BaseURL       *url.URL
	UserAgent     string
	Common        Service
	Policies      *PolicyService
	Watches       *WatchService
	Permissions   *PermissionService

	Token string
}

var (
	defaultRetryWaitMin = 1 * time.Second
	defaultRetryWaitMax = 30 * time.Second
	defaultRetryMax     = 4
)

func DefaultRetryPolicy(resp *http.Response, err error) (bool, error) {
	if err != nil {
		return true, err
	}

	if resp.StatusCode == 0 || resp.StatusCode >= 500 || resp.StatusCode == 401 {
		return true, nil
	}

	return false, nil
}

func DefaultBackoff(min, max time.Duration, attemptNum int, resp *http.Response) time.Duration {
	mult := math.Pow(2, float64(attemptNum)) * float64(min)
	sleep := time.Duration(mult)
	if float64(sleep) != mult || sleep > max {
		sleep = max
	}
	return sleep
}

// NewClient creates a Client from a provided base url for an artifactory instance and a http Client
func NewClient(baseURL string, httpClient *http.Client, token string) (*Client, error) {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	baseEndpoint, err := url.Parse(baseURL)

	if err != nil {
		return nil, err
	}

	if !strings.HasSuffix(baseEndpoint.Path, "/") {
		baseEndpoint.Path += "/"
	}

	c := &Client{Client: httpClient, BaseURL: baseEndpoint, UserAgent: utils.UserAgent}
	c.Common.Client = c

	c.Policies = (*PolicyService)(&c.Common)
	c.Watches = (*WatchService)(&c.Common)
	c.Permissions = (*PermissionService)(&c.Common)
	c.RetryMax = defaultRetryMax
	c.RetryWaitMin = defaultRetryWaitMin
	c.RetryMax = defaultRetryMax
	c.CheckForRetry = DefaultRetryPolicy
	c.Backoff = DefaultBackoff
	c.Token = token

	return c, nil
}

// NewRequest creates an API request. A relative URL can be provided in urlStr, in which case it is resolved relative to the BaseURL
// of the Client. Relative URLs should always be specified without a preceding slash. If specified, the value pointed to
// by body is included as the request body.
func (c *Client) NewRequest(method, urlStr string, body io.Reader) (*http.Request, error) {
	u, err := c.BaseURL.Parse(path.Join(c.BaseURL.Path, urlStr))
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, u.String(), body)
	if err != nil {
		return nil, err
	}

	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}
	return req, nil
}

// NewJSONEncodedRequest is a wrapper around Client.NewRequest which encodes the body as a JSON object
func (c *Client) NewJSONEncodedRequest(method, urlStr string, body interface{}) (*http.Request, error) {
	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		enc := json.NewEncoder(buf)
		enc.SetEscapeHTML(false)
		err := enc.Encode(body)
		if err != nil {
			return nil, err
		}
	}

	req, err := c.NewRequest(method, urlStr, buf)
	if err != nil {
		return nil, err
	}
	if body != nil {
		req.Header.Set("Content-Type", utils.MediaTypeJson)
	}
	return req, nil
}

// NewURLEncodedRequest is a wrapper around Client.NewRequest which encodes the body with URL encoding
func (c *Client) NewURLEncodedRequest(method, urlStr string, body interface{}) (*http.Request, error) {
	var buf io.Reader
	if body != nil {
		urlVals, err := query.Values(body)
		if err != nil {
			return nil, err
		}
		buf = strings.NewReader(urlVals.Encode())
	}

	req, err := c.NewRequest(method, urlStr, buf)
	if err != nil {
		return nil, err
	}
	if body != nil {
		req.Header.Set("Content-Type", utils.MediaTypeJson)
	}
	return req, nil
}

func checkResponse(r *http.Response) error {
	if c := r.StatusCode; 200 <= c && c <= 299 || c == http.StatusUnauthorized {
		return nil
	}
	errorResponse := &ErrorResponse{Response: r}
	data, err := ioutil.ReadAll(r.Body)
	if err == nil && data != nil {
		err = json.Unmarshal(data, errorResponse)
		if err != nil || len(errorResponse.Errors) == 0 {
			return fmt.Errorf(string(data))
		}
	}

	return errorResponse
}

// Do executes a give request with the given context. If the parameter v is a writer the body will be written to it in
// raw format, else v is assumed to be a struct to unmarshal the body into assuming JSON format. If v is nil then the
// body is not read and can be manually parsed from the response
func (c *Client) Do(ctx context.Context, req *http.Request, v interface{}) (*http.Response, error) {

	i := 0
	for {

		req = req.WithContext(ctx)
		req.Header.Set(utils.HeaderApiToken, fmt.Sprintf(utils.TokenType, c.Token))
		req.Header.Set(utils.HeaderContenType, utils.MediaTypeJson)
		var code int

		resp, err := c.Client.Do(req)
		if err != nil {
			// If we got an error, and the context has been canceled,
			// the context's error is probably more useful.
			select {
			case <-ctx.Done():
				return nil, ctx.Err()
			default:
			}

			if e, ok := err.(*url.Error); ok {
				if url2, err := url.Parse(e.URL); err == nil {
					e.URL = url2.String()
					return nil, e
				}
			}

			return nil, err
		}
		defer resp.Body.Close()
		err = checkResponse(resp)

		checkOK, checkErr := c.CheckForRetry(resp, err)
		if err != nil {
			// even though there was an error, we still return the response
			// in case the caller wants to inspect it further
			return resp, err
		}

		if !checkOK {
			if checkErr != nil {
				err = checkErr
			}
			return resp, err
		}

		// We're going to retry, consume any response to reuse the connection.
		if err == nil {
			errRenew := c.RenewXrayToken(req)
			if errRenew != nil {
				return nil, errRenew
			}
		}

		remain := c.RetryMax - i
		if remain == 0 {
			break
		}

		wait := c.Backoff(c.RetryWaitMin, c.RetryWaitMax, i, resp)
		desc := fmt.Sprintf("%s %s", req.Method, req.URL)
		if code > 0 {
			desc = fmt.Sprintf("%s (status: %d)", desc, code)
		}

		log.Printf("DEBUG %s: retrying in %s (%d left)", desc, wait, remain)
		time.Sleep(wait)
		i++
	}

	return nil, fmt.Errorf("%s %s giving up after %d attempts", req.Method, req.URL, c.RetryMax+1)
}

func (c *Client) RenewXrayToken(req *http.Request) error {

	client, err := NewClientWithToken(fmt.Sprintf(utils.HttpProto+c.BaseURL.Host), c.Credentials)
	if err != nil {
		log.Printf("Error when generating Xray Token: %v", err)
		return err
	}
	log.Printf("Xray Token has been renewed")

	c.Client.Transport = client.Client.Transport
	c.Token = client.Token
	req.Header.Del(utils.HeaderApiToken)
	req.Header.Set(utils.HeaderApiToken, fmt.Sprintf(utils.TokenType, c.Token))
	log.Printf("Request header has been changed with new Token")

	return nil
}

func NewClientWithToken(BaseURL string, credentials *types.GetToken) (*Client, error) {

	// Connect to XRAY to ask for a Token
	url := fmt.Sprintf(BaseURL + "/api/v1/auth/token")

	payload, err := json.Marshal(credentials)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	var jsonStr = []byte(payload)

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonStr))
	req.Header.Set(utils.HeaderContenType, utils.MediaTypeJson)

	clientForToken := &http.Client{}
	resp, err := clientForToken.Do(req)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer resp.Body.Close()

	// Parsing to get Token
	var getTokenBody types.GetTokenBody
	err = json.NewDecoder(resp.Body).Decode(&getTokenBody)
	if err != nil {
		log.Println(err)
	}

	client, err := NewClient(BaseURL, &http.Client{}, getTokenBody.Token)
	if err != nil {
		log.Printf("\nerror: %v\n", err)
	}

	client.Credentials = credentials

	return client, nil

}

// ErrorResponse reports one or more errors caused by an API request.
type ErrorResponse struct {
	Response *http.Response `json:"-"`                // HTTP response that caused this error
	Errors   []Status       `json:"errors,omitempty"` // Individual errors
}

// Status is the individual error provided by the API
type Status struct {
	Status  int    `json:"status"`  // Validation error status code
	Message string `json:"message"` // Message describing the error. Errors with Code == "custom" will always have this set.
}

func (e *Status) Error() string {
	return fmt.Sprintf("%d error caused by %s", e.Status, e.Message)
}

func (r *ErrorResponse) Error() string {
	return fmt.Sprintf("%v %v: %d %+v", r.Response.Request.Method, r.Response.Request.URL,
		r.Response.StatusCode, r.Errors)
}

// Bool is a helper routine that allocates a new bool value
// to store v and returns a pointer to it.
func Bool(v bool) *bool { return &v }

// Int is a helper routine that allocates a new int value
// to store v and returns a pointer to it.
func Int(v int) *int { return &v }

// Int64 is a helper routine that allocates a new int64 value
// to store v and returns a pointer to it.
func Int64(v int64) *int64 { return &v }

// String is a helper routine that allocates a new string value
// to store v and returns a pointer to it.
func String(v string) *string { return &v }
