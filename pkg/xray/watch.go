package xray

import (
	"context"
	"net/http"
)

type WatchService Service

type Watch struct {
	GeneralData GeneralData `json:"general_data"`
	ProjectResources ProjectResources `json:"project_resources"`
	AssignedPolicies []AssignedPolicies `json:"assigned_policies"`
}

type GeneralData struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Active      bool   `json:"active"`
}

type ProjectResources struct {
	Resources []Resources `json:"resources"`
}

type Resources struct {
	Type     string `json:"type"`
	BinMgrID string `json:"bin_mgr_id"`
	RepoType string `json:"repo_type"`
	Name     string `json:"name"`
	Filters  []Filters `json:"filters"`
}

type Filters struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

type AssignedPolicies struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

func (s *WatchService) CreateWatch(ctx context.Context, watch *Watch) (*http.Response, error){
	return s.create(ctx, watch)
}

func (s *WatchService) create(ctx context.Context, v interface{}) (*http.Response, error) {
	path := "api/v2/watches"
	req, err := s.Client.NewJSONEncodedRequest(http.MethodPost, path, v)
	if err != nil {
		return nil, err
	}

	return s.Client.Do(ctx, req, nil)
}

func (s *WatchService) UpdateWatch(ctx context.Context, watch *Watch, nameOfWatch string) (*http.Response, error){
	return s.update(ctx, watch, nameOfWatch)
}

func (s *WatchService) update(ctx context.Context, v interface{}, nameOfWatch string) (*http.Response, error) {
	path := "api/v2/watches/" + nameOfWatch
	req, err := s.Client.NewJSONEncodedRequest(http.MethodPut, path, v)
	if err != nil {
		return nil, err
	}

	return s.Client.Do(ctx, req, nil)
}

