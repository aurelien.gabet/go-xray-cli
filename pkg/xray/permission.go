package xray

import (
	"context"
	"net/http"
)

type PermissionService Service

type Permission struct {
	Name      string                `json:"name"`
	Scope     string                `json:"scope"`
	Groups    []Groups              `json:"groups"`
	Resources []ResourcesPermission `json:"resources"`
}

type ResourcesPermission struct {
	Type          string `json:"type"`
	Name          string `json:"name"`
	ArtifactoryID string `json:"artifactory_id"`
	PkgType       string `json:"pkg_type"`
}

type Groups struct {
	Name  string   `json:"name"`
	Roles []string `json:"roles"`
}

func (s *PermissionService) CreatePermission(ctx context.Context, permission *Permission) (*http.Response, error) {
	return s.create(ctx, permission)
}

func (s *PermissionService) create(ctx context.Context, v interface{}) (*http.Response, error) {
	path := "api/v1/permissions"
	req, err := s.Client.NewJSONEncodedRequest("POST", path, v)
	if err != nil {
		return nil, err
	}

	return s.Client.Do(ctx, req, nil)
}

func (s *PermissionService) DeletePermission(ctx context.Context, permission *Permission, nameOfPermission string) (*http.Response, error) {
	return s.delete(ctx, permission, nameOfPermission)
}

func (s *PermissionService) delete(ctx context.Context, v interface{}, nameOfPermission string) (*http.Response, error) {
	path := "api/v1/permissions/" + nameOfPermission
	req, err := s.Client.NewJSONEncodedRequest("DELETE", path, v)
	if err != nil {
		return nil, err
	}

	return s.Client.Do(ctx, req, nil)
}
