package utils

const (
	UserAgent        = "go-xray-cli"
	HttpProto        = "https://"
	HeaderApiToken   = "Authorization"
	HeaderContenType = "Content-Type"
	MediaTypeJson    = "application/json"
	TokenType        = "Bearer %s"
)
