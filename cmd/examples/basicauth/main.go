package main

import (
	"context"
	"fmt"
	"gitlab.com/aurelien.gabet/go-xray-cli/pkg/xray"
	"net/http"
	"os"
	"reflect"
)

func createPolicy(client *xray.Client, policy *xray.Policy) (*http.Response, error){

	resp, err := client.Policies.CreatePolicies(context.Background(), policy)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
	return resp, err
}

func updatePolicy(client *xray.Client, policy *xray.Policy, name string) {

	resp, err := client.Policies.UpdatePolicies(context.Background(), policy, name)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
}

func createWatch(client *xray.Client, watch *xray.Watch) (*http.Response, error) {

	resp, err := client.Watches.CreateWatch(context.Background(), watch)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
	return resp, err
}

func updateWatch(client *xray.Client, watch *xray.Watch, name string) (*http.Response, error) {

	resp, err := client.Watches.UpdateWatch(context.Background(), watch, name)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
	return resp, err
}


func main() {

	tp := xray.BasicAuthTransport{
		Username: os.Getenv("XRAY_USERNAME"),
		Password: os.Getenv("XRAY_PASSWORD"),
	}

	client, err := xray.NewClient(os.Getenv("XRAY_URL"), tp.Client())
	if err != nil {
		fmt.Printf("\nerror: %v\n", err)
		return
	}

	policyToCreate := &xray.Policy{
		Name: "aug-e4-docker-scratch-intranet",
		Type: "security",
		Rules: []xray.Rule{xray.Rule{
			Name:     "aug-e4-docker-scratch-intranet",
			Priority: 1,
			Criteria: xray.Criteria{
				MinSeverity: "High",
			},
			Actions: xray.Action{
				Mails:     []string{"aurelien.gabet@example.com"},
				FailBuild: false,
				BlockDownload: xray.BlockDownload{
					Unscanned: false,
					Active:    false,
				},
			}},
		},
		Description: "aug test",
	}

	policyToUpdate := &xray.Policy{
		Name: "aug-e4-docker-scratch-intranet",
		Type: "security",
		Rules: []xray.Rule{xray.Rule{
			Name:     "aug-e4-docker-scratch-intranet",
			Priority: 1,
			Criteria: xray.Criteria{
				MinSeverity: "High",
			},
			Actions: xray.Action{
				Mails:     []string{"aurelien.gabet2@example.com"},
				FailBuild: false,
				BlockDownload: xray.BlockDownload{
					Unscanned: false,
					Active:    false,
				},
			}},
		},
		Description: "aug test",
	}

	resp, err := createPolicy(client, policyToCreate)
	fmt.Println(resp)
	if err.Error() == `{"error":"Policy already exists"}` {
		updatePolicy(client, policyToUpdate,"aug-e4-docker-scratch-intranet")
	} else {
		fmt.Println(reflect.TypeOf(err.Error()))
	}


	filters := &xray.Filters{
		Type:  "regex",
		Value: ".*",
	}

	resources := &xray.Resources{
		Type:     "repository",
		BinMgrID: "arti-rct",
		RepoType: "local",
		Name:     "aug-e4-docker-scratch-intranet",
		Filters:  []xray.Filters{*filters},
	}

	assignedPolicy := &xray.AssignedPolicies{
		Name: "aug-e4-docker-scratch-intranet",
		Type: "security",
	}

	watchToCreate := &xray.Watch{
		GeneralData: xray.GeneralData{
			Name:        "aug-e4-docker-scratch-intranet",
			Description: "This is a new watch created using API V2",
			Active:      true,
		},
		ProjectResources: xray.ProjectResources{
			Resources: []xray.Resources{*resources},
		},
		AssignedPolicies: []xray.AssignedPolicies{*assignedPolicy},
	}

	watchToUpdate := &xray.Watch{
		GeneralData: xray.GeneralData{
			Name:        "aug-e4-docker-scratch-intranet",
			Description: "This is a new watch created using API V2",
			Active:      true,
		},
		ProjectResources: xray.ProjectResources{
			Resources: []xray.Resources{*resources},
		},
		AssignedPolicies: []xray.AssignedPolicies{*assignedPolicy},
	}

	resp, err = createWatch(client,watchToCreate)
	if err.Error() == `{"error":"Watch already exists"}` {
		updateWatch(client, watchToUpdate, "aug-e4-docker-scratch-intranet")
	}
}
